#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <sensor_msgs/CameraInfo.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc/imgproc.hpp>

#include "ladybug.h"
#include "ladybugstream.h"

using namespace std;

static volatile int running_ = 1;


LadybugContext m_context;
LadybugDataFormat m_dataFormat;
//camera config settings
float m_frameRate;
bool m_isFrameRateAuto;
unsigned int m_jpegQualityPercentage;
int m_serial_number;
int m_external_gpio_trigger;
std::string m_frame_id;
image_transport::Publisher pub[LADYBUG_NUM_CAMERAS];

static void signalHandler(int)
{
  running_ = 0;
  ros::shutdown();
}

void publishImage(cv_bridge::CvImage &image, image_transport::Publisher &image_pub, long int &count, size_t camera_id, ros::Time in_time)
{
  sensor_msgs::Image msg;
  //publish*******************
  image.toImageMsg(msg);
  msg.header.seq = count;
  std::string frame = m_frame_id + std::to_string(camera_id);
  msg.header.frame_id = frame;
  msg.header.stamp.sec = in_time.sec;
  msg.header.stamp.nsec = in_time.nsec;
  msg.encoding = "bgr8";

  image_pub.publish(msg);
}

void displayTriggerModeInfo( LadybugTriggerModeInfo *triggerModeInfo)
{
  ROS_INFO( "--- Trigger mode info ---");
  ROS_INFO( " bPresent %d", triggerModeInfo->bPresent);
  ROS_INFO( " bOnOffSupported %d", triggerModeInfo->bOnOffSupported);
  ROS_INFO( " bPolaritySupported %d", triggerModeInfo->bPolaritySupported);
  ROS_INFO( " bReadOutSupported %d", triggerModeInfo->bReadOutSupported);
  ROS_INFO( " bSoftwareTriggerSupported %d", triggerModeInfo->bSoftwareTriggerSupported);
  ROS_INFO( " bValueReadable %d", triggerModeInfo->bValueReadable);
  ROS_INFO( " uiModeMask %X", triggerModeInfo->uiModeMask);
  for ( int i = 0; i < 16; i++){
    if ( ( ( triggerModeInfo->uiModeMask >> i) & 0x01) == 1){
      ROS_INFO( "  supports trigger mode %d", 15 - i);
    }
  }
  ROS_INFO( " uiSourceMask %X", triggerModeInfo->uiSourceMask);
}

void displayTriggerMode( LadybugTriggerMode *triggerMode)
{
  ROS_INFO( "--- Trigger mode ---");
  ROS_INFO( " bOnOff %d", triggerMode->bOnOff);
  ROS_INFO( " uiMode %d", triggerMode->uiMode);
  ROS_INFO( " uiParameter %d", triggerMode->uiParameter);
  ROS_INFO( " uiPolarity %d", triggerMode->uiPolarity);
  ROS_INFO( " uiSource %d", triggerMode->uiSource);
}

LadybugError init_camera(bool use_external_trigger, unsigned int gpio_trigger)
{
  LadybugError error;
  error = ladybugCreateContext(&m_context);
  if (error != LADYBUG_OK)
  {
    throw std::runtime_error("Unable to create Ladybug context.");
  }

  LadybugCameraInfo enumeratedCameras[16];
  unsigned int numCameras = 16;

  error = ladybugBusEnumerateCameras(m_context, enumeratedCameras, &numCameras);
  if (error != LADYBUG_OK)
  {
    return error;
  }

  cout << "Cameras detected: " << numCameras << endl << endl;

  if (numCameras == 0)
  {
    ROS_INFO("Insufficient number of cameras detected. ");
    return LADYBUG_FAILED;
  }

  error = ladybugInitializeFromSerialNumber(m_context, m_serial_number);
  if (error != LADYBUG_OK)
  {
    ROS_ERROR("Camera not found with the serial number: %d", m_serial_number);
    return error;
  }

  LadybugCameraInfo camInfo;
  error = ladybugGetCameraInfo(m_context, &camInfo);
  if (error != LADYBUG_OK)
  {
    return error;
  }

  ROS_INFO("Camera information: ");

  ROS_INFO("Base s/n: %d", camInfo.serialBase);
  ROS_INFO("Head s/n: %d", camInfo.serialHead);
  ROS_INFO("Model: %s", camInfo.pszModelName);
  ROS_INFO("Sensor: %s", camInfo.pszSensorInfo);
  ROS_INFO("Vendor: %s", camInfo.pszVendorName);
  ROS_INFO("Bus / Node: %d ,%d", camInfo.iBusNum, camInfo.iNodeNum);

  if (use_external_trigger)
  {
    LadybugTriggerMode triggerMode;

    triggerMode.bOnOff = true;
    triggerMode.uiSource = gpio_trigger;
    triggerMode.uiMode = 0;
    triggerMode.uiParameter = 0;
    triggerMode.uiPolarity = 1;
    error = ladybugSetTriggerMode(m_context, &triggerMode);
    if (error != LADYBUG_OK)
    {
      ROS_ERROR("ladybugSetTriggerMode error");
      return error;
    }

    error = ladybugGetTriggerMode(m_context, &triggerMode);
    if (error != LADYBUG_OK)
    {
      ROS_ERROR("ladybugGetTriggerMode error");
      return error;
    }
    displayTriggerMode(&triggerMode);
  }

  LadybugTriggerModeInfo triggerModeInfo;

  error = ladybugGetTriggerModeInfo( m_context, &triggerModeInfo );
  if (error != LADYBUG_OK)
  {
    ROS_ERROR("ladybugGetTriggerModeInfo error");
    return error;
  }
  displayTriggerModeInfo(&triggerModeInfo);

  switch (camInfo.deviceType)
  {
    case LADYBUG_DEVICE_LADYBUG3:
    {
      m_dataFormat = LADYBUG_DATAFORMAT_RAW8;
      m_frameRate = 16.0f;
      m_isFrameRateAuto = true;
      m_jpegQualityPercentage = 80;
    }
      break;

    case LADYBUG_DEVICE_LADYBUG5:
    case LADYBUG_DEVICE_LADYBUG5P:
    {
      m_dataFormat = LADYBUG_DATAFORMAT_RAW8;
      m_frameRate = 10.0f;
      m_isFrameRateAuto = false;
      m_jpegQualityPercentage = 80;
    }
      break;

    default:
      assert(false);
      break;
  }

  return error;
}

LadybugError start_camera()
{
  LadybugError error;
  error = ladybugStartLockNext(m_context, m_dataFormat);
  if (error != LADYBUG_OK)
  {
    return error;
  }

  error = ladybugSetAbsPropertyEx(m_context, LADYBUG_FRAME_RATE, false, true, m_isFrameRateAuto, m_frameRate);
  if (error != LADYBUG_OK)
  {
    return error;
  }

  error = ladybugSetJPEGQuality(m_context, m_jpegQualityPercentage);
  if (error != LADYBUG_OK)
  {
    return error;
  }

  // Perform a quick test to make sure images can be successfully acquired
  for (int i = 0; i < 10; i++)
  {
    LadybugImage tempImage;
    error = ladybugLockNext(m_context, &tempImage);
  }

  error = ladybugUnlockAll(m_context);
  if (error != LADYBUG_OK)
  {
    return error;
  }

  return error;
}

LadybugError stop_camera()
{
  LadybugError cameraError = ladybugStop(m_context);
  if (cameraError != LADYBUG_OK)
  {
    ROS_INFO("Error: Unable to stop camera (%s)", ladybugErrorToString(cameraError));
  }

  LadybugTriggerMode triggerMode;

  triggerMode.bOnOff = false;
  cameraError = ladybugSetTriggerMode(m_context, &triggerMode);
  if (cameraError != LADYBUG_OK)
  {
    ROS_ERROR("ladybugSetTriggerMode error");
  }

  cameraError = ladybugDestroyContext( &m_context);

  return cameraError;
}

LadybugError acquire_image(LadybugImage &image)
{
  return ladybugLockNext(m_context, &image);
}

LadybugError unlock_image(unsigned int bufferIndex)
{
  return ladybugUnlock(m_context, bufferIndex);
}

int main(int argc, char **argv)
{
  ////ROS STUFF
  ros::init(argc, argv, "ladybug_camera");
  ros::NodeHandle n;
  ros::NodeHandle private_nh("~");
  image_transport::ImageTransport it(n);

  signal(SIGTERM, signalHandler);//detect closing

  /////////////////////////////
  //Config camera
  m_dataFormat = LADYBUG_DATAFORMAT_RAW8;
  m_isFrameRateAuto = false;
  m_jpegQualityPercentage = 80;

  if (private_nh.getParam("fps", m_frameRate) && m_frameRate > 0.0f && m_frameRate <= 40.0f)
  {
    ROS_INFO("Ladybug fps > %.0f", m_frameRate);
  } else
  {
    ROS_INFO("Ladybug framerate must be (0,40]. Defaulting to 10 ");
    m_frameRate = 10.0f;
  }

  private_nh.param<std::string>("frame_id", m_frame_id, "camera");
  // Initialize ladybug camera
  private_nh.param<int>("serial_number", m_serial_number, 0);

  bool external_trigger = false;
  private_nh.param<bool>("use_external_trigger", external_trigger, false);
  ROS_INFO("use_external_trigger: %d", external_trigger);

  int gpio_trigger = 0;
  private_nh.param<int>("external_trigger_gpio", gpio_trigger, 0);
  if (gpio_trigger < 0 || gpio_trigger >= 7)
  {
    ROS_WARN("Invalid GPIO %d, resetting to 0");
    gpio_trigger = 0;
  }
  ROS_INFO("external_trigger_gpio: %d", gpio_trigger);

  const LadybugError grabberInitError = init_camera(external_trigger, gpio_trigger);
  if (LADYBUG_OK != grabberInitError)
  {
    ROS_INFO("Error: Failed to initialize camera (%s). Terminating...", ladybugErrorToString(grabberInitError));
    return -1;
  }

  LadybugCameraInfo camInfo;
  if (LADYBUG_OK != ladybugGetCameraInfo(m_context, &camInfo))
  {
    ROS_INFO("Error: Failed to get camera information. Terminating...");
    return -1;
  }

  if (!external_trigger)
  {
    const LadybugError startError = start_camera();
    if (startError != LADYBUG_OK)
    {
      ROS_INFO("Error: Failed to start camera (%s). Terminating...", ladybugErrorToString(startError));
      return -1;
    }
    ROS_INFO("Successfully started ladybug camera and stream");
  }
  else
  {
    const LadybugError error = ladybugSetGrabTimeout( m_context, 1000);
    if (error != LADYBUG_OK)
    {
      ROS_INFO("Error: ladybugSetGrabTimeout. Terminating...");
      return -1;
    }
    // Start streaming
    const LadybugError acquisitionError = ladybugStart( m_context, m_dataFormat);
    if (acquisitionError != LADYBUG_OK)
    {
      ROS_INFO("ladybugStart Error (%s). Trying to continue..", ladybugErrorToString(acquisitionError));
    }
  }

  for (int i = 0; i < LADYBUG_NUM_CAMERAS; i++)
  {
    std::string topic(std::string("image_raw"));

    topic = "lady" + std::to_string(i) + "/" + topic;

    pub[i] = it.advertise(topic, 1);
    ROS_INFO("Publishing.. %s", topic.c_str());
  }
  //////////////////

  //start camera
  ros::Rate loop_rate(m_frameRate); // Hz Ladybug works at fps
  long int count = 0;
  while (running_ && ros::ok())
  {
    LadybugImage currentImage;

    if (!external_trigger)
    {
      const LadybugError acquisitionError = acquire_image(currentImage);
      if (acquisitionError != LADYBUG_OK)
      {
        ROS_INFO("Failed to acquire image. Error (%s). Trying to continue..", ladybugErrorToString(acquisitionError));
        continue;
      }
    }
    else
    {
      const LadybugError acquisitionError = ladybugGrabImage( m_context, &currentImage);
      if (acquisitionError != LADYBUG_OK)
      {
        ROS_INFO("Failed to acquire image. Error (%s). Trying to continue..", ladybugErrorToString(acquisitionError));
        continue;
      }
    }
    ros::Time received_time = ros::Time::now();

    // convert to OpenCV Mat
    cv::Size size(currentImage.uiFullCols, currentImage.uiFullRows);

    cv::Mat full_size;
    for (size_t i = 0; i < LADYBUG_NUM_CAMERAS; i++)
    {
      cv::Mat rawImage(size, CV_8UC1, currentImage.pData + (i * size.width * size.height));

      cv::rotate(rawImage, rawImage, cv::ROTATE_90_CLOCKWISE);

      cv::Mat color_image;
      cv::cvtColor(rawImage, color_image, CV_BayerGB2BGR);
      cv::resize(color_image, color_image, cv::Size(), 0.5, 0.5);

      cv_bridge::CvImage cv_image;
      cv_image.image = color_image;

      publishImage(cv_image, pub[i], count, i, received_time);
    }
    unlock_image(currentImage.uiBufferIndex);
    if (!external_trigger)
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
    else
    {
      ros::spinOnce();
    }
    count++;
  }

  cout << "Stopping ladybug_camera..." << endl;

  // Shutdown
  stop_camera();

  ROS_INFO("ladybug_camera stopped");

  return 0;
}

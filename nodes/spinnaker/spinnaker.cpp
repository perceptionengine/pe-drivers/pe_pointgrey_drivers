
/*
 * Copyright 2020 Perception Engine. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <thread>
#include <sstream>
#include <iostream>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <std_msgs/Header.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/core/core.hpp>

#include "SpinGenApi/SpinnakerGenApi.h"
#include "Spinnaker.h"

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;

class SpinnakerCamera
{
public:
  SpinnakerCamera();
  ~SpinnakerCamera();
  void spin();

private:
  ros::NodeHandle nh_, private_nh_;
  image_transport::ImageTransport* image_transport_;
  image_transport::Publisher image_pub_;
  Spinnaker::SystemPtr system_;
  Spinnaker::CameraList camList_;
  CameraPtr pCameraPointer_;

  // config
  int width_;
  int height_;
  double fps_, min_shutter_speed_ms_;
  int dltl_, serial_number_;
  std::string format_;
  std::string frame_id_;
  bool use_external_trigger_;
  int external_trigger_gpio_;
  double auto_min_gain_, auto_max_gain_, manual_gain_;

  Spinnaker::GenApi::INodeMap* node_map_;


  void set_frame_rate(CameraPtr camera_ptr, double fps);
  void set_external_trigger_source(CameraPtr camera_ptr, int external_gpio);
  void enable_trigger(CameraPtr camera_ptr);
  void disable_trigger(CameraPtr camera_ptr);
  void set_continuous_acquisition(CameraPtr camera_ptr);
  void set_pixel_format(CameraPtr camera_ptr);
  void set_dltl(CameraPtr camera_ptr, int dltl);
  void set_manual_shutter_speed(CameraPtr camera_ptr, double shutter_speed_ms);
  void set_auto_shutter_speed(CameraPtr camera_ptr);
  void set_auto_gain(CameraPtr camera_ptr, double min_gain, double max_gain);
  void set_manual_gain(CameraPtr camera_ptr, double in_gain);
};

SpinnakerCamera::SpinnakerCamera()
  : nh_()
  , private_nh_("~")
  , system_(Spinnaker::System::GetInstance())
  , camList_(system_->GetCameras())
  , width_(0)
  , height_(0)
  , fps_(0)
  , dltl_(0)
{
  private_nh_.param("width", width_, 0);
  private_nh_.param("height", height_, 0);
  private_nh_.param("fps", fps_, 20.0);
  private_nh_.param("dltl", dltl_, 100000000);
  private_nh_.param<int>("serial_number", serial_number_, 0);
  private_nh_.param<std::string>("frame_id", frame_id_, "camera");
  private_nh_.param<bool>("use_external_trigger", use_external_trigger_, false);
  private_nh_.param<int>("external_trigger_gpio", external_trigger_gpio_, 0);
  private_nh_.param<double>("min_shutter_speed_ms", min_shutter_speed_ms_, 0);

  private_nh_.param<double>("auto_min_gain", auto_min_gain_, 0.);
  private_nh_.param<double>("auto_max_gain", auto_max_gain_, 18.);

  private_nh_.param<double>("manual_gain", manual_gain_, -1.);


  unsigned int num_cameras = camList_.GetSize();
  if (serial_number_> 0)
  {
    num_cameras = 1;
  }

  ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Number of cameras detected: " << num_cameras);
  if (width_ > 0 and height_ > 0)
  {
    ROS_INFO_STREAM("[" << ros::this_node::getName() << "] width: " << width_);
    ROS_INFO_STREAM("[" << ros::this_node::getName() << "] weight: " << height_);
  }
  ROS_INFO_STREAM("[" << ros::this_node::getName() << "] fps: " << fps_);
  ROS_INFO_STREAM("[" << ros::this_node::getName() << "] dltl: " << dltl_);
  ROS_INFO_STREAM("[" << ros::this_node::getName() << "] serial_number: " << serial_number_);
  ROS_INFO_STREAM("[" << ros::this_node::getName() << "] frame_id: " << frame_id_);

  if (num_cameras < 1)
  {
    ROS_ERROR("[%s] Error: This program requires at least 1 camera.", ros::this_node::getName().c_str());
    camList_.Clear();
    system_->ReleaseInstance();
    std::exit(-1);
  }
  image_transport_ = new image_transport::ImageTransport(nh_);

  try
  {
    if(serial_number_ <= 0)
    {
      ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Get first camera available");
      pCameraPointer_ = camList_.GetByIndex(0);
    }
    else
    {
      ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Camera by serial: " << serial_number_);
      pCameraPointer_ = camList_.GetBySerial(std::to_string(serial_number_));
    }
    if (pCameraPointer_ == nullptr)
    {
      ROS_ERROR_STREAM("[" << ros::this_node::getName() << "] Invalid pointer.");
      camList_.Clear();
      system_->ReleaseInstance();
      std::exit(-1);
    }
    pCameraPointer_->Init();
    node_map_ = &pCameraPointer_->GetNodeMap();
    ///////////////////////                 resolution
    if (width_ > 0 and height_ > 0)
    {
      pCameraPointer_->Width.SetValue(width_);
      pCameraPointer_->Height.SetValue(height_);
    }
    ///////////////////////                 dltl                   /////////////////////////////
    set_dltl(pCameraPointer_, dltl_);

    ///////////////////////                 FrameRate                   /////////////////////////////
    set_frame_rate(pCameraPointer_, fps_);

    ///////////////////////                 PixelFormat                 /////////////////////////////
    set_pixel_format(pCameraPointer_);

    ///////////////////////                 AcquisitionMode                 /////////////////////////////
    set_continuous_acquisition(pCameraPointer_);

    ///////////////////////                 External Trigger                  /////////////////////////////
    disable_trigger(pCameraPointer_);
    if (use_external_trigger_)
    {
      set_external_trigger_source(pCameraPointer_, external_trigger_gpio_);
      enable_trigger(pCameraPointer_);
    }

    ///////////////////////                 ShutterSpeed                  /////////////////////////////
    
    if (min_shutter_speed_ms_ > 0)
    {
      set_manual_shutter_speed(pCameraPointer_, min_shutter_speed_ms_);
    }
    else
    {
      set_auto_shutter_speed(pCameraPointer_);
    }
    /////// Manual Gain /////////////////////
    if (manual_gain_ >= 0.)
    {
    	ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Given Manual Gain. Ignoring Automatic settings");
    	set_manual_gain(pCameraPointer_, manual_gain_);
    }
    else
    {
    	ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Automatic Gain with range (" << auto_min_gain_ << ", " << auto_max_gain_ <<")");
    	set_auto_gain(pCameraPointer_, auto_min_gain_, auto_max_gain_);
    }

    ///////////////////////                 create publisher
    std::string topic("image_raw");
    image_pub_ = image_transport_->advertise(topic, 1);
  }
  catch (Spinnaker::Exception& e)
  {
    ROS_ERROR("[%s] Error: %s", ros::this_node::getName().c_str(), e.what());
  }
}

SpinnakerCamera::~SpinnakerCamera()
{

  pCameraPointer_->EndAcquisition();
  pCameraPointer_->DeInit();
  pCameraPointer_ = nullptr;
  ROS_INFO("[%s] Shutting down camera ", ros::this_node::getName().c_str());
  node_map_->InvalidateNodes();
  node_map_ = nullptr;
  camList_.Clear();
  system_->ReleaseInstance();
}

void SpinnakerCamera::spin()
{

  if (pCameraPointer_ != nullptr)
  {
    pCameraPointer_->BeginAcquisition();
    ROS_INFO("[%s] Started acquisition", ros::this_node::getName().c_str());
  }

  while (ros::ok())
  {
    try
    {
      ImagePtr pResultImage = pCameraPointer_->GetNextImage();
      if (pResultImage->IsIncomplete())
      {
        ROS_WARN("[%s] [camera] Image incomplete with image status %d", ros::this_node::getName().c_str(),
                 (int)pResultImage->GetImageStatus());
      }
      else
      {
        ros::Time receive_time = ros::Time::now();
        try {
          uint64_t camera_time_nsec = pResultImage->GetTimeStamp();
          ros::Time camera_receive_time = ros::Time().fromNSec(camera_time_nsec);
          receive_time = camera_receive_time;
        }
        catch(...){
          ROS_WARN_STREAM_THROTTLE(1, "Could not get time from camera. Using system time.");
        }
        // create opencv mat
        int pixel_format = pResultImage->GetPixelFormat();
        std::string encoding_pattern;
        int cv_encoding = CV_8UC1;
        switch (pixel_format)
        {
          case PixelFormatEnums::PixelFormat_BayerRG8:
            encoding_pattern = "bayer_rggb8";
            break;
          case PixelFormatEnums::PixelFormat_BayerGR8:
            encoding_pattern = "bayer_grbg8";
            break;
          case PixelFormatEnums::PixelFormat_BayerGB8:
            encoding_pattern = "bayer_gbrg8";
            break;
          case PixelFormatEnums::PixelFormat_BayerBG8:
            encoding_pattern = "bayer_bggr8";
            break;
          case PixelFormatEnums::PixelFormat_RGB8:
            encoding_pattern = "rgb8";
            cv_encoding = CV_8UC3;
            break;
          case PixelFormatEnums::PixelFormat_BGR8:
            encoding_pattern = "bgr8";
            cv_encoding = CV_8UC3;
            break;
          default:
            encoding_pattern = "mono8";
        }

        // create and publish ros message
        std_msgs::Header header;
        header.stamp = receive_time;
        header.frame_id = frame_id_;

        cv::Mat camera_image = cv::Mat(pResultImage->GetHeight(),
                                       pResultImage->GetWidth(),
                                       cv_encoding,
                                       pResultImage->GetData());

        sensor_msgs::ImagePtr image_msg = cv_bridge::CvImage(header,
                                                             encoding_pattern,
                                                             camera_image).toImageMsg();

        image_pub_.publish(image_msg);
      }
      pResultImage->Release();
    }
    catch (Spinnaker::Exception& e)
    {
      ROS_ERROR("[%s] Error: %s", ros::this_node::getName().c_str(), e.what());
    }
  }

}

void SpinnakerCamera::set_external_trigger_source(CameraPtr camera_ptr, int external_gpio)
{
  CEnumerationPtr ptrTriggerSource = camera_ptr->GetNodeMap().GetNode("TriggerSource");
  if (!IsAvailable(ptrTriggerSource) || !IsWritable(ptrTriggerSource))
  {
    ROS_ERROR("[%s]Unable to set trigger mode (node retrieval). Aborting...", ros::this_node::getName().c_str());
    return;
  }
  std::string gpio_string("Line");
  gpio_string+=std::to_string(external_gpio);
  CEnumEntryPtr ptrTriggerSourceHardware = ptrTriggerSource->GetEntryByName(gpio_string.c_str());
  if (!IsAvailable(ptrTriggerSourceHardware) || !IsReadable(ptrTriggerSourceHardware))
  {
    ROS_ERROR("[%s]Unable to set trigger mode (enum entry retrieval). Aborting...", ros::this_node::getName().c_str());
    return;
  }

  ptrTriggerSource->SetIntValue(ptrTriggerSourceHardware->GetValue());

  ROS_INFO("[%s]Trigger source set to hardware...", ros::this_node::getName().c_str());
}

void SpinnakerCamera::disable_trigger(CameraPtr camera_ptr)
{
  CEnumerationPtr ptrTriggerMode = camera_ptr->GetNodeMap().GetNode("TriggerMode");
  if (!IsAvailable(ptrTriggerMode) || !IsReadable(ptrTriggerMode))
  {
    ROS_ERROR("[%s]Unable to disable trigger mode (node retrieval). Aborting...", ros::this_node::getName().c_str());
    return;
  }

  CEnumEntryPtr ptrTriggerModeOff = ptrTriggerMode->GetEntryByName("Off");
  if (!IsAvailable(ptrTriggerModeOff) || !IsReadable(ptrTriggerModeOff))
  {
    ROS_ERROR("[%s]Unable to disable trigger mode (enum entry retrieval). Aborting...", ros::this_node::getName().c_str());
    return;
  }

  ptrTriggerMode->SetIntValue(ptrTriggerModeOff->GetValue());

  ROS_INFO("[%s]Trigger mode disabled...", ros::this_node::getName().c_str());
}

void SpinnakerCamera::enable_trigger(CameraPtr camera_ptr)
{
  CEnumerationPtr ptrTriggerMode =camera_ptr->GetNodeMap().GetNode("TriggerMode");
  CEnumEntryPtr ptrTriggerModeOn = ptrTriggerMode->GetEntryByName("On");
  if (!IsAvailable(ptrTriggerModeOn) || !IsReadable(ptrTriggerModeOn))
  {
    ROS_ERROR("[%s]Unable to enable trigger mode (enum entry retrieval). Aborting...", ros::this_node::getName().c_str());
    return;
  }

  ptrTriggerMode->SetIntValue(ptrTriggerModeOn->GetValue());
  ROS_INFO("[%s]Trigger mode turned back on...", ros::this_node::getName().c_str());
}

void SpinnakerCamera::set_pixel_format(CameraPtr camera_ptr)
{
  CEnumerationPtr ptrPixelFormat = camera_ptr->GetNodeMap().GetNode("PixelFormat");
  if (IsAvailable(ptrPixelFormat) && IsWritable(ptrPixelFormat))
  {
    ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Current pixel Format"
                        << ptrPixelFormat->GetCurrentEntry()->GetSymbolic());
    /*gcstring pixel_format = format_.c_str();

    CEnumEntryPtr ptrPixelFormatSetup = ptrPixelFormat->GetEntryByName(pixel_format);
    if (IsAvailable(ptrPixelFormatSetup) && IsReadable(ptrPixelFormatSetup))
    {
      int64_t pixelFormatSetup = ptrPixelFormatSetup->GetValue();
      ptrPixelFormat->SetIntValue(ptrPixelFormatSetup);

      ROS_INFO_STREAM("[" << __APP_NAME__ << "] Pixel format set to " <<
                                        ptrPixelFormat->GetCurrentEntry()->GetSymbolic());
    }
    else
    {
      ROS_WARN("[%s] %s pixel format not available. Using default.", __APP_NAME__, format_.c_str());
    }*/
  }
  else
  {
    ROS_WARN("[%s] Pixel format cannot be changed on this camera. Using default.", ros::this_node::getName().c_str());
  }
}

void SpinnakerCamera::set_frame_rate(CameraPtr camera_ptr, double fps)
{

  CFloatPtr ptrAcquisitionFrameRate = camera_ptr->GetNodeMap().GetNode("AcquisitionFrameRate");
  CBooleanPtr ptrAcquisitionFrameRateEnable = camera_ptr->GetNodeMap().GetNode("AcquisitionFrameRateEnable");
  CEnumerationPtr ptrAcquisitionFrameRateAuto = camera_ptr->GetNodeMap().GetNode("AcquisitionFrameRateAuto");
  if (IsAvailable(ptrAcquisitionFrameRateAuto) && IsWritable(ptrAcquisitionFrameRateAuto))
  {
    CEnumEntryPtr ptrAcquisitionFrameRateAutoOff = ptrAcquisitionFrameRateAuto->GetEntryByName("Off");
    if (IsAvailable(ptrAcquisitionFrameRateAutoOff) && IsReadable(ptrAcquisitionFrameRateAutoOff))
    {
      int64_t FrameRateAutoOff = ptrAcquisitionFrameRateAutoOff->GetValue();
      ptrAcquisitionFrameRateAuto->SetIntValue(FrameRateAutoOff);
      ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Updated FrameRateAuto to Off");
    }
    else
    {
      ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Cannot update FrameRateAuto to Off");
    }
  }
  if (IsAvailable(ptrAcquisitionFrameRateEnable) && IsWritable(ptrAcquisitionFrameRateEnable))
  {
    ptrAcquisitionFrameRateEnable->SetValue(true);
  }
  if (IsAvailable(ptrAcquisitionFrameRate) && IsWritable(ptrAcquisitionFrameRate))
  {
    ptrAcquisitionFrameRate->SetValue(fps);
    ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Set FrameRate to " << fps);
  }
  else
  {
    ROS_WARN("[%s] This camera does not support AcquisitionFrameRate, using default value.", ros::this_node::getName().c_str());
  }
}

void SpinnakerCamera::set_continuous_acquisition(CameraPtr camera_ptr)
{
  CEnumerationPtr ptrAcquisitionMode = camera_ptr->GetNodeMap().GetNode("AcquisitionMode");
  if (!IsAvailable(ptrAcquisitionMode) || !IsWritable(ptrAcquisitionMode))
  {
    ROS_FATAL("[%s] Unable to set acquisition mode to continuous (node retrieval; "
                  "camera ",
              ros::this_node::getName().c_str());
  }

  ///////////////////////                 ContinuousMode                  /////////////////////////////
  CEnumEntryPtr ptrAcquisitionModeContinuous = ptrAcquisitionMode->GetEntryByName("Continuous");
  if (!IsAvailable(ptrAcquisitionModeContinuous) || !IsReadable(ptrAcquisitionModeContinuous))
  {
    ROS_FATAL("[%s] Unable to set acquisition mode to continuous (entry "
                  "'continuous' retrieval. Aborting...",
              ros::this_node::getName().c_str());
  }

  int64_t acquisitionModeContinuous = ptrAcquisitionModeContinuous->GetValue();
  ptrAcquisitionMode->SetIntValue(acquisitionModeContinuous);

  ROS_INFO("[%s] acquisition mode set to continuous...", ros::this_node::getName().c_str());
}

void SpinnakerCamera::set_dltl(CameraPtr camera_ptr, int dltl)
{
  CEnumerationPtr ptrDeviceType = camera_ptr->GetTLDeviceNodeMap().GetNode("DeviceType");

  if (IsAvailable(ptrDeviceType) && ptrDeviceType->GetCurrentEntry()->GetSymbolic() == "GEV")
  {
    ///////////////////////                 DeviceLinkThroughputLimit   /////////////////////////////
    CIntegerPtr ptrDeviceLinkThroughputLimit = camera_ptr->GetNodeMap().GetNode("DeviceLinkThroughputLimit");
    if (IsAvailable(ptrDeviceLinkThroughputLimit) && IsWritable(ptrDeviceLinkThroughputLimit))
    {
      ROS_INFO_STREAM("[" << ros::this_node::getName()
                          << "] DeviceLinkThroughputLimit: " << ptrDeviceLinkThroughputLimit->GetValue());
      ptrDeviceLinkThroughputLimit->SetValue(dltl);
    }
    else
    {
      ROS_WARN("[%s] This camera does not support DeviceLinkThroughputLimit, using default value.", ros::this_node::getName().c_str());
    }
  }
}

void SpinnakerCamera::set_auto_shutter_speed(CameraPtr camera_ptr)
{
  try
  {

    if (!IsReadable(camera_ptr->ExposureAuto) || !IsWritable(camera_ptr->ExposureAuto))
    {
      ROS_ERROR("[%s] Unable to enable automatic exposure (node retrieval). Non-fatal error...", ros::this_node::getName().c_str());
      return;
    }

    camera_ptr->ExposureAuto.SetValue(ExposureAuto_Continuous);

    ROS_INFO("[%s] Automatic exposure enabled...", ros::this_node::getName().c_str());
  }
  catch (Spinnaker::Exception& e)
  {
    ROS_ERROR("[%s] Error: %s", ros::this_node::getName().c_str(),  e.what());
    return;
  }
}

void SpinnakerCamera::set_manual_shutter_speed(CameraPtr camera_ptr, double shutter_speed_ms)
{
  try
  {
    if (!IsReadable(camera_ptr->ExposureAuto) || !IsWritable(camera_ptr->ExposureAuto))
    {
      ROS_ERROR("[%s] Unable to disable automatic exposure. Aborting...", ros::this_node::getName().c_str());
      return;
    }

    camera_ptr->ExposureAuto.SetValue(ExposureAuto_Off);

    ROS_INFO("[%s] Automatic exposure disabled...", ros::this_node::getName().c_str());

    if (!IsReadable(camera_ptr->ExposureTime) || !IsWritable(camera_ptr->ExposureTime))
    {
      ROS_ERROR("[%s] Unable to set exposure time. Aborting...", ros::this_node::getName().c_str());
      return;
    }

    // Ensure desired exposure time does not exceed the maximum
    const double exposureTimeMax = camera_ptr->ExposureTime.GetMax();
    double exposureTimeToSet = shutter_speed_ms*1000;

    if (exposureTimeToSet > exposureTimeMax)
    {
      exposureTimeToSet = exposureTimeMax;
      ROS_WARN("[%s]Given shutter speed value (%f ms) is larger than maximum allowed. Setting it to max available: %f ms",
               ros::this_node::getName().c_str(), shutter_speed_ms, exposureTimeToSet/1000);
    }

    camera_ptr->ExposureTime.SetValue(exposureTimeToSet);

    ROS_INFO_STREAM("[" << ros::this_node::getName() << "]"
                        << std::fixed << "Shutter time set to " << exposureTimeToSet/1000 << " ms..." );
  }
  catch (Spinnaker::Exception& e)
  {
    ROS_ERROR("[%s] Error: %s", ros::this_node::getName().c_str(), e.what());
  }
}

void SpinnakerCamera::set_auto_gain(CameraPtr camera_ptr, double min_gain, double max_gain)
{
  try
  {
    CEnumerationPtr ptrGainAuto = camera_ptr->GetNodeMap().GetNode("GainAuto");
    if (IsAvailable(ptrGainAuto) && IsWritable(ptrGainAuto))
    {
      CEnumEntryPtr ptrGainAutoContinuous = ptrGainAuto->GetEntryByName("Continuous");
      if (IsAvailable(ptrGainAutoContinuous) && IsReadable(ptrGainAutoContinuous))
      {
        ptrGainAuto->SetIntValue(static_cast<int64_t>(ptrGainAutoContinuous->GetValue()));
        ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Automatic gain enabled.");

        CFloatPtr ptrGainAutoUpper = camera_ptr->GetNodeMap().GetNode("AutoExposureGainUpperLimit");
        CFloatPtr ptrGainAutoLower = camera_ptr->GetNodeMap().GetNode("AutoExposureGainLowerLimit");
        
        if (IsAvailable(ptrGainAutoUpper) && IsReadable(ptrGainAutoUpper)
        	&& IsAvailable(ptrGainAutoLower) && IsReadable(ptrGainAutoLower))
        {
          CFloatPtr ptrGain = camera_ptr->GetNodeMap().GetNode("Gain");
          double gainMax = ptrGain->GetMax();
          double gainMin = ptrGain->GetMin();
          double maxGainToSet = max_gain;
          double minGainToSet = min_gain;

          if (max_gain > gainMax)
          {
            maxGainToSet = gainMax;
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Given Input Gain (" << max_gain << ") is larger than the maximum allowed gain "
            << gainMax << " using max allowed instead.");
          }
          if (min_gain < gainMin)
          {
            minGainToSet = gainMin;
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Given Input Gain (" << max_gain << ") is smaller than the minimum allowed gain "
            << gainMin << " using min allowed instead.");
          }
          ptrGainAutoUpper->SetValue(maxGainToSet);
          ptrGainAutoLower->SetValue(minGainToSet);
          ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Automatic gain lower and upper limit set (" << minGainToSet << ", " << maxGainToSet << ")");
        }
      }
      else
      {
      	ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Automatic gain not available.");
      }
    }
  }
  catch (Spinnaker::Exception &e)
  {
    ROS_ERROR("[%s] Error: %s", ros::this_node::getName().c_str(), e.what());
  }
}

void SpinnakerCamera::set_manual_gain(CameraPtr camera_ptr, double in_gain)
{
  try
  {
  	CEnumerationPtr ptrGainAuto = camera_ptr->GetNodeMap().GetNode("GainAuto");
    if (IsAvailable(ptrGainAuto) && IsWritable(ptrGainAuto))
    {
      CEnumEntryPtr ptrGainAutoOff = ptrGainAuto->GetEntryByName("Off");
      if (IsAvailable(ptrGainAutoOff) && IsReadable(ptrGainAutoOff))
      {
        ptrGainAuto->SetIntValue(static_cast<int64_t>(ptrGainAutoOff->GetValue()));
        ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Automatic gain disabled.");
      }
    }
    CFloatPtr ptrGain = camera_ptr->GetNodeMap().GetNode("Gain");
    if (!IsAvailable(ptrGain) || !IsWritable(ptrGain))
    {
      ROS_INFO_STREAM("[" << ros::this_node::getName() << "]"<< "Unable to set gain (node retrieval).");
      return;
    }

    double gainMax = ptrGain->GetMax();
    double gainToSet = in_gain;

    if (in_gain > gainMax)
    {
      gainToSet = gainMax;
      ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Input Gain is larger than maximum allowed gain "
      << gainMax << " using max allowed instead.");
    }

    ptrGain->SetValue(gainToSet);
    ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Gain set to "<< gainToSet);
  }
  catch (Spinnaker::Exception &e)
  {
    ROS_ERROR("[%s] Error: %s", ros::this_node::getName().c_str(), e.what());
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pe_spinnaker");
  SpinnakerCamera node;
  node.spin();
  return 0;
}

/*
 * Copyright 2020 Perception Engine. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
  This program requires ROS and Flycapture SDK installed
  Author: Abraham Monrroy (amonrroy@ertl.jp)
  Initial version 		2014-11-14
  Added signal handler 		2015-05-01
  Added CameraInfo msg 		2015-05-01
 */

#include <iostream>
#include <stdlib.h>
#include <FlyCapture2.h>
#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>

#include <sensor_msgs/CameraInfo.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include <signal.h>

static volatile int running = 1;

static void signal_handler(int)
{
  running = 0;
  ros::shutdown();
}

void print_camera_info(FlyCapture2::CameraInfo *info)
{
  ROS_INFO_STREAM("\n*** CAMERA INFORMATION ***");
  ROS_INFO_STREAM("\tSerial number       - " << info->serialNumber);
  ROS_INFO_STREAM("\tCamera model        - " << info->modelName);
  ROS_INFO_STREAM("\tCamera vendor       - " << info->vendorName);
  ROS_INFO_STREAM("\tSendor              - " << info->sensorInfo);
  ROS_INFO_STREAM("\tResolution          - " << info->sensorResolution);
  ROS_INFO_STREAM("\tFirmware version    - " << info->firmwareVersion);
  ROS_INFO_STREAM("\tFirmware build time - " << info->firmwareBuildTime);
}

void print_image_settings(
    const FlyCapture2::Format7ImageSettings &image_settings,
    const unsigned int &packet_size,
    const float &percentage
)
{
  ROS_INFO_STREAM("Image settings: " << std::endl);
  ROS_INFO_STREAM("\tMode: " << image_settings.mode);
  ROS_INFO_STREAM("\tPixel Format: 0x" << std::hex << image_settings.pixelFormat << std::dec);
  ROS_INFO_STREAM("\tOffset X: " << image_settings.offsetX);
  ROS_INFO_STREAM("\tOffset Y: " << image_settings.offsetY);
  ROS_INFO_STREAM("\tWidth: " << image_settings.width);
  ROS_INFO_STREAM("\tHeight: " << image_settings.height);
  ROS_INFO_STREAM("Packet size: " << packet_size << " (" << percentage << "%)");
}

void print_format7_info(const FlyCapture2::Format7Info &info, bool supported)
{
  ROS_INFO_STREAM("supported: " << supported);
  ROS_INFO_STREAM("mode: " << info.mode);
  ROS_INFO_STREAM("maxWidth: " << info.maxWidth);
  ROS_INFO_STREAM("maxHeight: " << info.maxHeight);
  ROS_INFO_STREAM("packetSize: " << info.packetSize);
  ROS_INFO_STREAM("percentage: " << info.percentage);
  ROS_INFO_STREAM("pixelFormatBitField: " << info.pixelFormatBitField);
}

void initialize_cameras(std::vector<FlyCapture2::Camera *> &cameras,
                        FlyCapture2::BusManager *bus_manger,
                        int camera_num,
                        FlyCapture2::Mode desired_mode,
                        FlyCapture2::PixelFormat desired_pixel_format,
                        int timeout_ms,
                        int camera_serial_number = 0,
                        float in_min_shutter_ms = 20.0f,
                        float in_gain = 29.0f)
{
  // Connect to all detected cameras and attempt to set them to
  // a common video mode and frame rate
  for (int i = 0; i < camera_num; i++)
  {
    FlyCapture2::Camera *camera = new FlyCapture2::Camera();

    FlyCapture2::PGRGuid guid;
    FlyCapture2::Error error;
    if (camera_serial_number > 0)
    {
      error = bus_manger->GetCameraFromSerialNumber(camera_serial_number, &guid);
    } else
    {
      error = bus_manger->GetCameraFromIndex(i, &guid);
    }
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    error = camera->Connect(&guid);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    FlyCapture2::EmbeddedImageInfo image_info;
    error = camera->GetEmbeddedImageInfo(&image_info);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    image_info.timestamp.onOff = false;
    error = camera->SetEmbeddedImageInfo(&image_info);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    // Get the camera information
    FlyCapture2::CameraInfo camera_info;
    error = camera->GetCameraInfo(&camera_info);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    //obtain working settings
    FlyCapture2::VideoMode default_video_mode;
    FlyCapture2::FrameRate default_frame_rate;

    error = camera->GetVideoModeAndFrameRate(&default_video_mode, &default_frame_rate);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    //try to set Format7, according to the desired mode and pixel format.
    FlyCapture2::Format7ImageSettings image_settings;
    bool supported = false;
    unsigned int packet_size;
    float percentage;

    FlyCapture2::Format7Info format7_info;
    format7_info.mode = desired_mode;

    error = camera->GetFormat7Info(&format7_info, &supported);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      std::exit(-1);
    }

    print_format7_info(format7_info, supported);

    if (supported)
    {
      error = camera->GetFormat7Configuration(&image_settings, &packet_size, &percentage);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        std::exit(-1);
      }

      image_settings.mode = desired_mode;
      image_settings.pixelFormat = desired_pixel_format;
      image_settings.offsetX = 0;
      image_settings.offsetY = 0;
      image_settings.width = format7_info.maxWidth;
      image_settings.height = format7_info.maxHeight;

      FlyCapture2::Format7PacketInfo packet_info;
      bool valid_settings = false;
      error = camera->ValidateFormat7Settings(&image_settings, &valid_settings, &packet_info);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        std::exit(-1);
      }
      packet_size = packet_info.recommendedBytesPerPacket;
      error = camera->SetFormat7Configuration(&image_settings, packet_size);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        std::exit(-1);
      }

      error = camera->GetFormat7Configuration(&image_settings, &packet_size, &percentage);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        std::exit(-1);
      }

      print_image_settings(image_settings, packet_size, percentage);
    } else
    {
      ROS_ERROR("[%s]Selected Mode not supported, using last working mode.", ros::this_node::getName().c_str());
    }

    FlyCapture2::FC2Config camera_config;
    error = camera->GetConfiguration(&camera_config);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      ROS_INFO("[%s]Could not read configuration from Camera", ros::this_node::getName().c_str());
    } else
    {
      if (timeout_ms > 0)
        camera_config.grabTimeout = timeout_ms;

      error = camera->SetConfiguration(&camera_config);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        ROS_INFO("[%s]Could not set configuration on Camera", ros::this_node::getName().c_str());
      }
    }

    FlyCapture2::Property prop;
    prop.type = FlyCapture2::SHUTTER;
    error = camera->GetProperty(&prop);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      ROS_ERROR("[%s]Camera does not support manual Shutter Speed", ros::this_node::getName().c_str());
    } else
    {
      if(in_min_shutter_ms > 0)
      {
        prop.autoManualMode = false;
        prop.absValue = in_min_shutter_ms;
      }
      else
      {
        prop.autoManualMode = true;
        ROS_INFO("[%s]Using auto shutter speed (given %f)", ros::this_node::getName().c_str(), in_min_shutter_ms);
      }
      prop.absControl = true;

      error = camera->SetProperty(&prop);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        ROS_ERROR("[%s]Could not set the specified Shutter Speed: %f ms", ros::this_node::getName().c_str(), in_min_shutter_ms);
      }
    }


    FlyCapture2::Property gain_prop;
    gain_prop.type = FlyCapture2::GAIN;
    error = camera->GetProperty(&gain_prop);
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      ROS_ERROR("[%s]Camera does not support gain", ros::this_node::getName().c_str());
    } else
    {
      if (in_gain < 0)
      {
        gain_prop.autoManualMode = false;
      }
      else
      {
        gain_prop.autoManualMode = false;
        gain_prop.absValue = in_gain;
      }
      gain_prop.absControl = true;

      error = camera->SetProperty(&gain_prop);
      ROS_INFO_STREAM("[" << ros::this_node::getName() << "] Gain set to " << in_gain);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        ROS_ERROR("[%s]Could not set the specified gain: %f ms", ros::this_node::getName().c_str(), gain_prop.absValue);
      }
    }



    print_camera_info(&camera_info);
    cameras.push_back(camera);
  }
}

bool check_external_trigger_support(FlyCapture2::Camera* camera, unsigned int external_trigger_gpio)
{
  // Check for external trigger support
  FlyCapture2::TriggerModeInfo triggerModeInfo;
  FlyCapture2::Error error;
  error = camera->GetTriggerModeInfo(&triggerModeInfo);
  if (error != FlyCapture2::PGRERROR_OK)
  {
    error.PrintErrorTrace();
    return false;
  }

  if (!triggerModeInfo.present)
  {
    ROS_ERROR("[%s]Camera does not support external trigger. ", ros::this_node::getName().c_str());
    return false;
  }

  // Get current trigger settings
  FlyCapture2::TriggerMode triggerMode;
  error = camera->GetTriggerMode(&triggerMode);
  if (error != FlyCapture2::PGRERROR_OK)
  {
    error.PrintErrorTrace();
    return false;
  }

  // Set camera to trigger mode 0
  triggerMode.onOff = true;
  triggerMode.mode = 0;
  triggerMode.parameter = 0;
  triggerMode.source = external_trigger_gpio;

  error = camera->SetTriggerMode(&triggerMode);
  if (error != FlyCapture2::PGRERROR_OK)
  {
    error.PrintErrorTrace();
    return false;
  }
  return true;
}

/*!
 * Get the number of cameras connected to the system
 * @param bus_manager Valid pointer to the BusManager
 * @return The number of detected cameras
 */
unsigned int get_num_cameras(FlyCapture2::BusManager *bus_manager)
{
  unsigned int cameras;
  FlyCapture2::Error error = bus_manager->GetNumOfCameras(&cameras);
  if (error != FlyCapture2::PGRERROR_OK)
  {
    error.PrintErrorTrace();
    std::exit(-1);
  }

  ROS_INFO("[%s]Number of cameras detected: %d", ros::this_node::getName().c_str(), cameras );

  if (cameras < 1)
  {
    ROS_ERROR("[%s]Error: This program requires at least 1 camera.", ros::this_node::getName().c_str());
    std::exit(-1);
  }
  return cameras;
}

/*!
 * Initialize the capture on all the cameras
 * @param cameras An array of valid pointers to the camera objects
 */
void start_capture(std::vector<FlyCapture2::Camera *> &cameras)
{
  for (auto *camera : cameras)
  {
    FlyCapture2::Error error = camera->StartCapture();
    if (error != FlyCapture2::PGRERROR_OK)
    {
      error.PrintErrorTrace();
      return;
    }
  }
}

/*!
 * Reads the params from the console
 * @param private_nh[in] Private ROS node handle
 * @param fps[out] Read value from the console double
 * @param mode[out] Read value from the console integer
 * @param format[out] Read value from the console raw or rgb
 * @param timeout[out] Read value from the console timeout in ms
 * @param camera_serial_number[out] Camera Serial number (if provided)
 * @param camera_frame[out] Camera Frame to store in ROS header
 * @param min_shutter_speed_ms[out] Camera shutter speed in mili seconds (Use 0 or negative to use set it to auto)
 * @param use_external_trigger[out] Wait for trigger, if true, otherwise free run capture.
 * @param external_trigger_gpio[out] GPIO pin where to expect for the trigger.
 */
void ros_get_params(const ros::NodeHandle &private_nh, int &fps, int &mode, std::string &format, int &timeout,
                    int &camera_serial_number, std::string &camera_frame, float &min_shutter_speed_ms,
                    bool &use_external_trigger, int &external_trigger_gpio, float &gain_db)
{
  if (private_nh.getParam("fps", fps))
  {
    ROS_INFO("[%s]fps set to %d", ros::this_node::getName().c_str(), fps);
  } else
  {
    fps = 20;
    ROS_INFO("[%s]No param received, defaulting fps to %d", ros::this_node::getName().c_str(), fps);
  }

  if (private_nh.getParam("mode", mode))
  {
    ROS_INFO("[%s]mode set to %d", ros::this_node::getName().c_str(), mode);
  } else
  {
    mode = 0;
    ROS_INFO("[%s]No param received, defaulting mode to %d", ros::this_node::getName().c_str(), mode);
  }

  if (private_nh.getParam("format", format))
  {
    ROS_INFO("[%s]format set to %s", ros::this_node::getName().c_str(), format.c_str());
  } else
  {
    format = "raw";
    ROS_INFO("[%s]No param received, defaulting format to %s", ros::this_node::getName().c_str(), format.c_str());
  }

  if (private_nh.getParam("timeout", timeout))
  {
    ROS_INFO("[%s]timeout set to %d ms", ros::this_node::getName().c_str(), timeout);
  } else
  {
    timeout = 1000;
    ROS_INFO("[%s]No param received, defaulting timeout to %d ms", ros::this_node::getName().c_str(), timeout);
  }
  if (private_nh.getParam("serial_number", camera_serial_number))
  {
    ROS_INFO("[%s]camera_serial_number %d ", ros::this_node::getName().c_str(), camera_serial_number);
  } else
  {
    camera_serial_number = 0;
    ROS_INFO("[%s]No param received, initializing all the available cameras", ros::this_node::getName().c_str());
  }
  if (private_nh.getParam("camera_frame", camera_frame))
  {
    ROS_INFO("[%s]camera frame set to %s ", ros::this_node::getName().c_str(), camera_frame.c_str());
  } else
  {
    camera_frame = "camera";
    ROS_INFO("[%s]No param received, defaulting camera_frame to %s", ros::this_node::getName().c_str(), camera_frame.c_str());
  }
  if (private_nh.getParam("min_shutter_speed_ms", min_shutter_speed_ms))
  {
    ROS_INFO("[%s]camera shutter speed set to %f ", ros::this_node::getName().c_str(), min_shutter_speed_ms);
  } else
  {
    min_shutter_speed_ms = 0;
    ROS_INFO("[%s]No param received, auto shutter speed", ros::this_node::getName().c_str());
  }

  if (private_nh.getParam("use_external_trigger", use_external_trigger))
  {
    ROS_INFO("[%s]use_external_trigger set to %d ", ros::this_node::getName().c_str(), use_external_trigger);
  } else
  {
    use_external_trigger = false;
    ROS_INFO("[%s]No param received,  camera set to free run acquisition.", ros::this_node::getName().c_str());
  }

  if (use_external_trigger && private_nh.getParam("external_trigger_gpio", external_trigger_gpio))
  {
    ROS_INFO("[%s]external_trigger_gpio set to pin: %d. Resetting timeout to 10s and fps to 20", ros::this_node::getName().c_str(), external_trigger_gpio);
    timeout = 10000;
    fps = 20;
  } else
  {
    use_external_trigger = false;
    ROS_INFO("[%s]No gpio param received.", ros::this_node::getName().c_str());
  }
  if (private_nh.getParam("gain_db", gain_db))
  {
    ROS_INFO("[%s]camera gain_db set to %f ", ros::this_node::getName().c_str(), gain_db);
  } else
  {
    gain_db = 0;
    ROS_INFO("[%s]No param received, auto gain", ros::this_node::getName().c_str());
  }

}

int main(int argc, char **argv)
{
  ////////////////POINT GREY CAMERA /////////////////////////////
  FlyCapture2::BusManager busMgr;

  ////ROS STUFF////
  ros::init(argc, argv, "pe_flycapture_node");
  ros::NodeHandle n;
  ros::NodeHandle private_nh("~");

  signal(SIGTERM, signal_handler);//detect closing

  int fps, camera_mode, timeout, camera_serial_number, external_trigger_gpio;
  float min_shutter_speed_ms, gain_db;
  bool use_external_trigger;
  std::string pixel_format, camera_frame;
  image_transport::ImageTransport it(n);

  ros_get_params(private_nh, fps, camera_mode, pixel_format, timeout, camera_serial_number, camera_frame,
                 min_shutter_speed_ms, use_external_trigger, external_trigger_gpio, gain_db);

  //
  FlyCapture2::Mode desired_mode;
  FlyCapture2::PixelFormat desired_pixel_format;

  desired_mode = (FlyCapture2::Mode) camera_mode;

  if (pixel_format == "rgb")
  {
    desired_pixel_format = FlyCapture2::PIXEL_FORMAT_RGB8;
  } else
  {
    desired_pixel_format = FlyCapture2::PIXEL_FORMAT_RAW8;
  }

  //init cameras
  int camera_num;
  std::vector<FlyCapture2::Camera *> cameras;
  if (camera_serial_number > 0)
  {
    camera_num = 1;
    initialize_cameras(cameras, &busMgr, camera_num, desired_mode, desired_pixel_format, timeout, camera_serial_number,
                       min_shutter_speed_ms);
  } else
  {
    camera_num = get_num_cameras(&busMgr);
    initialize_cameras(cameras, &busMgr, camera_num, desired_mode, desired_pixel_format, timeout, 0,
                       min_shutter_speed_ms, gain_db);
  }
  if(use_external_trigger)
  {
    for (size_t i = 0; i < cameras.size(); i++)
    {
      if (!check_external_trigger_support(cameras[i], external_trigger_gpio))
      {
        ROS_ERROR_STREAM("[" << ros::this_node::getName() << "]Camera " << i << " does not support triggering. Finishing...");
        return -1;
      }
    }
  }

  image_transport::Publisher pub[camera_num];
  ros::Publisher camera_info_pub;

  for (int i = 0; i < camera_num; i++)
  {
    std::string topic(std::string("image_raw"));

    if (camera_num > 1)
    {
      topic = camera_frame + std::to_string(i) + "/" + topic;
    }
    pub[i] = it.advertise(topic, 10);
    ROS_INFO("[%s]Publishing.. %s", ros::this_node::getName().c_str(), topic.c_str());
  }

  start_capture(cameras);

  ROS_INFO("[%s]Capturing  %d", ros::this_node::getName().c_str(), camera_num);

  if(use_external_trigger)
  {
    ROS_INFO("[%s]Waiting for trigger on GPIO  %d", ros::this_node::getName().c_str(), external_trigger_gpio);
  }

  int count = 0;
  ros::Rate loop_rate(fps); // Hz
  while (running && private_nh.ok())
  {
    int i = 0;
    for (auto *camera : cameras)
    {
      FlyCapture2::Image image;
      FlyCapture2::Error error = camera->RetrieveBuffer(&image);
      if (error != FlyCapture2::PGRERROR_OK)
      {
        error.PrintErrorTrace();
        continue;
      }

      // check encoding pattern
      std::string encoding_pattern;
      int cv_encoding = CV_8UC1;
      switch (image.GetBayerTileFormat())
      {
        case FlyCapture2::RGGB:
          encoding_pattern = "bayer_rggb8";
          break;
        case FlyCapture2::GRBG:
          encoding_pattern = "bayer_grbg8";
          break;
        case FlyCapture2::GBRG:
          encoding_pattern = "bayer_gbrg8";
          break;
        case FlyCapture2::BGGR:
          encoding_pattern = "bayer_bggr8";
          break;
        default:
          encoding_pattern = "bg8";
      }

      // create and publish ros message
      std_msgs::Header header;
      header.stamp = ros::Time::now();
      header.frame_id = camera_frame + std::to_string(i);

      cv::Mat camera_image = cv::Mat(image.GetRows(),
                                     image.GetCols(),
                                     cv_encoding,
                                     image.GetData());

      sensor_msgs::ImagePtr image_msg = cv_bridge::CvImage(header,
                                                           encoding_pattern,
                                                           camera_image).toImageMsg();

      pub[i].publish(image_msg);
      i++;
    }

    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }

  //close cameras
  for (auto *camera : cameras)
  {
    camera->StopCapture();
    camera->Disconnect();
    delete camera;
  }

  ROS_INFO("[%s]Camera node closed correctly", ros::this_node::getName().c_str());
  return 0;
}

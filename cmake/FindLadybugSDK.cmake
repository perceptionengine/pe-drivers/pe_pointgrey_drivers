include(FindPackageHandleStandardArgs)
unset(LADYBUGSDK_FOUND)
unset(LADYBUGSDK_INCLUDE_DIRS)
unset(LADYBUGSDK_LIBRARIES)

find_path(LADYBUGSDK_INCLUDE_DIRS NAMES
  ladybug.h
  PATHS
  /usr/include/ladybug/
  /usr/local/include/ladybug/
)

find_library(LADYBUGSDK_LIBRARIES NAMES ladybug
  PATHS
  /usr/lib/ladybug
  /usr/local/lib/ladybug
)

find_package_handle_standard_args(LADYBUGSDK DEFAULT_MSG LADYBUGSDK_INCLUDE_DIRS LADYBUGSDK_LIBRARIES)

if (${LADYBUGSDK_INCLUDE_DIRS} AND ${LADYBUGSDK_LIBRARIES})
  set(LADYBUGSDK_FOUND 1)
endif ()

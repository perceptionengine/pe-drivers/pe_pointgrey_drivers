# Perception Engine FLIR Camera Drivers Package

### Docker
```
docker build --rm -t TAG -f Dockerfile .

docker save -o pe_gain.tar pe_pointgrey_drivers:gain

docker load < pe_gain.tar
```

## Requirements
* FlyCapture SDK and/or Spinnaker SDK provided by Point Grey.

---

## FLIR Flycapture Cameras

### How to launch
* From a sourced terminal:\
`roslaunch pe_pointgrey_drivers flycapture.launch`

### Parameters available

|Parameter| Type| Description|
----------|-----|--------
|`fps`|*integer* |Defines the frames per second at which to attempt image stream acquisition.|
|`mode`|*integer*|Camera Mode - please check your camera for valid modes (0,1,2,...,31). |
|`format`|*string*|Pixel Format, which can be either `raw` or `rgb`. `raw` will publish the default bayer format according to your camera sensor. Both modes have 8 bits per pixel per channel.|
|`timeout`|*integer*|Timeout in miliseconds. Default 1000 ms.|
|`serial_number`|*string*|This will launch only the camera defined by the provded serial number, instead of launching all the available cameras.|
|`shutter_speed_ms`|*float*|Camera shutter Speed in mili seconds (Default 0, auto shutter speed).|
|`gain_db`|*float*|gain between 0 and MAX, where MAX is the max gain in db given by the sensor.|

## Ladybug

### How to launch
* From a sourced terminal:\
`roslaunch pe_pointgrey_drivers ladybug.launch`


### Parameters available

|Parameter| Type| Description|
----------|-----|--------
|`serial_number`|*string*|Camera Serial number. if not set will try to connect to all(Default 0).|


## Spinnaker

### How to launch
* From a sourced terminal:\
`roslaunch autoware_pointgrey_drivers spinnaker.launch`
* From Runtime manager:\
Sensing Tab -> Cameras -> PointGrey Spinnaker

### Parameters available

|Parameter| Type| Description|
----------|-----|--------
|`fps`|*integer* |Frame per second (Default 60).|
|`width`|*integer*|Image width of the stream (Default 1440).|
|`height`|*integer*|Image height of the camera (Default 1080).|
|`dltl`|*integer*|DeviceLinkThroughputLimit (Default 100000000).|
|`serial_number`|*string*|Camera Serial number. if not set will try to connect to all(Default 0).|
|`frame_id`|*string*||
|`use_external_trigger`|*int*||
|`external_trigger_gpio`*int*|||
|`min_shutter_speed_ms`|||